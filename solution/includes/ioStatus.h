#include <stdint.h>
#pragma once

/*  deserializer   */
enum read_status {
	READ_OK,
	READ_INVALID_SIGNATURE,
	READ_INVALID_BITS,
	READ_INVALID_HEADER
};


/*  serializer   */
enum  write_status {
	WRITE_OK,
	WRITE_ERROR
};
