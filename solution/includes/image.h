#include <stdint.h>
#pragma once

struct pixel { uint8_t b, g, r; };

struct image {
	uint64_t width, height;
	struct pixel* data;
};

struct image malloc_image(uint64_t width, uint64_t height);

void realloc_image(uint64_t width, uint64_t height, struct image* img);


