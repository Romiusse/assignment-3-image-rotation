#include <stdint.h>

#include "image.h"
#include "ioStatus.h"

#define BMP_TYPE 19778
#define BF_RESERVED 0
#define BI_SIZE 40
#define BI_PLANES 1
#define BI_COMPRESSION 0
#define BI_X_PIXELS_PER_METER 1
#define BI_Y_PIXELS_PER_METER 1
#define BI_CLR_USED 0
#define BI_CLR_IMPORTANT 0

#pragma pack(push, 1)
struct bmp_header
{
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};
#pragma pack(pop)

size_t calculate_paddings(uint32_t width);

struct bmp_header init_header(uint32_t width, uint32_t height);

enum read_status from_bmp(FILE* in, struct image* img);

enum write_status to_bmp(FILE* out, struct image const* img);
