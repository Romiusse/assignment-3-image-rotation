#include <stdint.h>
#pragma once

struct image rotate0(struct image img);

struct image rotate90(struct image img);

struct image rotate(struct image img, int angle);

struct image rotate180(struct image img);

struct image rotate270(struct image img);
