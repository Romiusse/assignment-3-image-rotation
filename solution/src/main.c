#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "../includes/bmp.h"
#include "../includes/errorcodes.h"
#include "../includes/image.h"
#include "../includes/ioStatus.h"
#include "../includes/transform.h"

int main( int argc, char** argv ) {
    if (argc != 4) {
        printf("not null args expected (inp_dir, out_dir, angle)\n");
        return WRONG_ARGS;
    }

    const char* input_image_dir = argv[1];
    const char* output_image_dir = argv[2];
    const int angle = atoi(argv[3]);
    
    FILE *input_image = fopen(input_image_dir, "rb");
    FILE *output_image = fopen(output_image_dir, "wb");

    if (input_image == NULL || output_image == NULL) {
            return ERROR_READ;
    }

    struct image img;
    from_bmp(input_image, &img);

    struct image new_img = rotate(img, angle);
    to_bmp(output_image, &new_img);
    free(new_img.data);

    fclose(input_image);
    fclose(output_image);

    return 0;
}
