#include <malloc.h>
#include <stdint.h>
#include <stdlib.h>

#include "../includes/errorcodes.h"
#include "../includes/image.h"

struct image malloc_image(uint64_t width, uint64_t height) {
	struct image img = (struct image) {width, height, malloc(width * height * sizeof(struct pixel))};
	if (img.data == NULL) {
		exit(ERROR_NO_FREE_SPASE);
	}
	return img;
}

void realloc_image(uint64_t width, uint64_t height, struct image* img) {

	img->data = realloc(img->data, width * height * sizeof(struct pixel));
	if (img->data == NULL) {
		exit(ERROR_NO_FREE_SPASE);
	}
}
