#include <math.h>
#include <stdint.h>
#include <stdlib.h>

#include "../includes/image.h"
#include "../includes/transform.h"


struct image rotate0(struct image img) {

	uint64_t height = img.height, width = img.width;
	struct pixel* data = img.data;

	struct image new_img = malloc_image(width, height);
	struct pixel* new_data = (&new_img)->data;

	for (uint64_t y = 0; y < height; y++) {
		for (uint64_t x = 0; x < width; x++) {
			new_data[y * width + x] = data[y * width + x];
		}
	}
	free(img.data);
	return new_img;

}

struct image rotate90(struct image img) {

	uint64_t height = img.height, width = img.width;
	struct pixel* data = img.data;

	struct image new_img = malloc_image(height, width);
	struct pixel* new_data = (&new_img)->data;

	for (uint64_t y = 0; y < height; y++) {
		for (uint64_t x = 0; x < width; x++) {
			new_data[(width - x - 1) * height + y] = data[y * width + x];
		}
	}
	free(img.data);
	return new_img;

}

struct image rotate180(struct image img) {

	uint64_t height = img.height, width = img.width;
	struct pixel* data = img.data;

	struct image new_img = malloc_image(width, height);
	struct pixel* new_data = (&new_img)->data;

	for (uint64_t y = 0; y < height; y++) {
		for (uint64_t x = 0; x < width; x++) {
			new_data[(height * width) - (y * width + x) - 1] = data[y * width + x];
		}
	}
	free(img.data);
	return new_img;

}

struct image rotate270(struct image img) {

	uint64_t height = img.height, width = img.width;
	struct pixel* data = img.data;

	struct image new_img = malloc_image(height, width);
	struct pixel* new_data = (&new_img)->data;

	for (uint64_t y = 0; y < height; y++) {
		for (uint64_t x = 0; x < width; x++) {
			new_data[x * height + (height - y - 1)] = data[y * width + x];
		}
	}
	free(img.data);
	return new_img;

}

struct image rotate(struct image img, int angle) {

	angle = (angle + 360) % 360;

	struct image out;

	if (angle == 90) out = rotate90(img);
	else if (angle == 180) out = rotate180(img);
	else if (angle == 270) out = rotate270(img);
	else out = rotate0(img);
	return out;

}
