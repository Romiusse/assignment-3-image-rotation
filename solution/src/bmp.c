#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "../includes/bmp.h"
#include "../includes/errorcodes.h"
#include "../includes/image.h"
#include "../includes/ioStatus.h"


size_t calculate_paddings(uint32_t width) {
	return (-1 * (width * sizeof(struct pixel))) % 4;
}

enum read_status from_bmp(FILE* in, struct image* img) {

	if (img == NULL) {
		return READ_INVALID_SIGNATURE;
	}

	struct bmp_header header;

	fread(&header, sizeof(struct bmp_header), 1, in);


	if (header.bfType != BMP_TYPE) {
		return READ_INVALID_HEADER;
	}

	*img = malloc_image(header.biWidth, header.biHeight);
	uint32_t height = header.biHeight, width = header.biWidth;
	struct pixel* data = img->data;

	for (uint64_t i = 0; i < height; i++) {
		if (fread(data + i * width, sizeof(struct pixel), width, in) != width) {
			return READ_INVALID_BITS;
		}
		int succses = fseek(in, (long)calculate_paddings(width), SEEK_CUR);
		if (succses != 0) {
			return READ_INVALID_BITS;
		}
	}

	return READ_OK;

}

struct bmp_header init_header(uint32_t width, uint32_t height) {

	struct bmp_header header;
	uint32_t img_size = height * (width * sizeof(struct pixel) + (long)calculate_paddings(width));

	header.bfType = BMP_TYPE;
	header.bfileSize = sizeof(struct bmp_header) + img_size;
	header.bfReserved = BF_RESERVED;
	header.bOffBits = sizeof(struct bmp_header);
	header.biSize = BI_SIZE;
	header.biWidth = width;
	header.biHeight = height;
	header.biPlanes = BI_PLANES;
	header.biBitCount = sizeof(struct pixel) * 8;
	header.biCompression = BI_COMPRESSION;
	header.biSizeImage = img_size;
	header.biXPelsPerMeter = BI_X_PIXELS_PER_METER;
	header.biYPelsPerMeter = BI_Y_PIXELS_PER_METER;
	header.biClrUsed = BI_CLR_USED;
	header.biClrImportant = BI_CLR_IMPORTANT;

	return header;

}

enum write_status to_bmp(FILE* out, struct image const* img) {

	struct pixel* data = img->data;

	struct bmp_header header = init_header((uint32_t)img->width, (uint32_t)img->height);

	uint32_t height = header.biHeight, width = header.biWidth;

	if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1) {
		return WRITE_ERROR;
	}

	for (uint64_t i = 0; i < height; i++) {
		if (fwrite(data + i * width, sizeof(struct pixel), width, out) != width) {
			return WRITE_ERROR;
		}
		int succses = fseek(out, (long)calculate_paddings(width), SEEK_CUR);
		if (succses != 0) {
			return WRITE_ERROR;
		}
	}

	return WRITE_OK;

}
